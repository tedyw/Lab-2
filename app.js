/*
#
# 	Filename: app.js
# 	Version: 0.1.0
#
# 	Description: Main javascript script collection.
# 	Author: Tedy Warsitha
# 	Web: http://codeorig.in/
#
*/

(function($){

function init(){
	$(".toggle").click(function(){
		$(".site-header").toggleClass("open");
	});
}

$(window).load(function(){
	init();
});

}(jQuery));	